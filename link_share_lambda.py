import json
import boto3
from time import gmtime, strftime
from botocore.client import Config

AWS_ACCESS_KEY_ID = ''
AWS_SECRET_ACCESS_KEY = ''
AWS_DEFAULT_REGION = ''
bucket_name = 'test-bara'

expiration_10_hours = 60 * 60 * 10
expiration_2_mins = 60 * 2
signature_version = 's3v4'

dynamodb = boto3.resource('dynamodb', region_name='us-east-2', aws_access_key_id=AWS_ACCESS_KEY_ID,
                          aws_secret_access_key=AWS_SECRET_ACCESS_KEY)
table = dynamodb.Table('LinkSharing')
now = strftime("%a, %d %b %Y %H:%M:%S +0000", gmtime())

s3_client = boto3.client('s3', region_name='us-east-1', aws_access_key_id=AWS_ACCESS_KEY_ID,
                         aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                         config=Config(signature_version=signature_version))


def get_presigned_url(object_key, expiration=expiration_10_hours):
    try:
        response = s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': bucket_name,
                                                            'Key': object_key},
                                                    ExpiresIn=expiration)

    except Exception as e:
        print(e)
        return None
    return response


def lambda_handler(event):
    name = event['filename']
    pw = event['password']
    authenticate = event['authenticate']
    if not authenticate:
        response = table.put_item(
            Item={
                'ID': name,
                'Password': pw
            })

        if pw:
            response = get_presigned_url('get_protected_file.html')
        else:
            response = get_presigned_url(name)
        return {
            'statusCode': 200,
            'body': json.dumps(response)
        }

    else:
        # authenticate
        response = table.get_item(
            Key={
                'ID': name
            }
        )
        file = response['Item']
        if file['Password'] == pw:
            response = get_presigned_url(name, expiration=expiration_2_mins)
            return {
                'statusCode': 200,
                'body': json.dumps(response)
            }
        else:
            return {
                'statusCode': 401,
                'body': json.dumps('Authentication Failed')
            }